run mvn clean install spring-boot:run

http://localhost:8000/swagger-ui.html

getReview - handles the procuring of data from yelp site. Accepts {id) which is the name of the restaurant.
detectFaces - handles Cloud Vision API
processReview - Response contains the emotions based on the Avatars of the reviewers from yelp.
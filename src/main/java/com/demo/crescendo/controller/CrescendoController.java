package com.demo.crescendo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.demo.crescendo.model.APIVisionRequestCollection;
import com.demo.crescendo.model.APIVisionResponseCollection;
import com.demo.crescendo.model.ProcessResponse;
import com.demo.crescendo.model.ReviewCollection;
import com.demo.crescendo.service.CrescendoService;

@RestController
public class CrescendoController {
	@Autowired
	CrescendoService yelpService;

	@RequestMapping(value = "/v1/review/{id}", produces = { "application/json" }, method = RequestMethod.GET)
	public ResponseEntity<ReviewCollection> getReview(@PathVariable String id) {
		return yelpService.getReview(id);
	}

	@RequestMapping(value = "/v1/review/", produces = { "application/json" }, consumes = {
			"application/json" }, method = RequestMethod.POST)
	public ResponseEntity<APIVisionResponseCollection> detectFaces(@RequestBody APIVisionRequestCollection body) {
		return yelpService.detectFaces(body);
	}

	@RequestMapping(value = "/v1/review/process/{id}", produces = { "application/json" }, method = RequestMethod.GET)
	public ResponseEntity<ProcessResponse> processReview(@PathVariable String id) {
		return yelpService.processReview(id);
	}

	@RequestMapping(value = "/v1/review/samp/{id}", produces = { "application/json" }, method = RequestMethod.GET)
	public ResponseEntity<String> samp(@PathVariable String id) {
		return new ResponseEntity<String>("samp", HttpStatus.OK);
//		return yelpService.processReview(id);
	}

}

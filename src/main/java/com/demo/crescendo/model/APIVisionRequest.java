package com.demo.crescendo.model;

import java.util.List;

import lombok.Data;

@Data
public class APIVisionRequest {
	private List<Feature> features;
	private Image image;
}

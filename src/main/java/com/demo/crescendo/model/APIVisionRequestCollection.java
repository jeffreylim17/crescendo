package com.demo.crescendo.model;

import java.util.List;

import lombok.Data;

@Data
public class APIVisionRequestCollection {
	private List<APIVisionRequest> requests;
}

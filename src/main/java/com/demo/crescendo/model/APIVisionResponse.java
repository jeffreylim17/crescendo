package com.demo.crescendo.model;

import java.util.List;

import lombok.Data;

@Data
public class APIVisionResponse {
	private List<FaceAnnotation> faceAnnotations;
}

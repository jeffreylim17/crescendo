package com.demo.crescendo.model;

import java.util.List;

import lombok.Data;

@Data
public class BoundingPoly {
	private List<Vertices> vertices;

}

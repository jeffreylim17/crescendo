package com.demo.crescendo.model;

import java.util.List;

import lombok.Data;

@Data
public class FaceAnnotation {
	private BoundingPoly boundingPoly;
	private FDBoundingPoly fdBoundingPoly;
	private List<Landmark> landmarks;
	private Integer rollAngle;
	private Integer panAngle;
	private Integer tiltAngle;
	private Integer detectionConfidence;
	private Integer landmarkingConfidence;
	private String joyLikelihood;
	private String sorrowLikelihood;
	private String angerLikelihood;
	private String surpriseLikelihood;
	private String underExposedLikelihood;
	private String blurredLikelihood;
	private String headwearLikelihood;
}

package com.demo.crescendo.model;

import lombok.Data;

@Data
public class Feature {
	private String type;
}

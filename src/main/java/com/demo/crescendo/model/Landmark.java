package com.demo.crescendo.model;

import lombok.Data;

@Data
public class Landmark {
	private String type;
	private Position position;
}

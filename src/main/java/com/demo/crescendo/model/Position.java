package com.demo.crescendo.model;

import lombok.Data;

@Data
public class Position {
	private Integer x;
	private Integer y;
	private Integer z;
}

package com.demo.crescendo.model;

import java.util.List;

import lombok.Data;

@Data
public class ProcessResponse {
	List<Response> response;
}

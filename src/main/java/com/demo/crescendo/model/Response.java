package com.demo.crescendo.model;

import lombok.Data;

@Data
public class Response {

	private User user;
	private String joyLikelihood;
	private String sorrowLikelihood;
	private String angerLikelihood;
	private String surpriseLikelihood;

}

package com.demo.crescendo.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class Review {

	@JsonProperty("id")
	private String id;

	@JsonProperty("rating")
	private Integer rating;

	@JsonProperty("user")
	private User user;

	@JsonProperty("text")
	private String text;

	@JsonProperty("time_created")
	private String timeCreated;

	@JsonProperty("url")
	private String url;

//	public String getId() {
//		return id;
//	}
//
//	public void setId(String id) {
//		this.id = id;
//	}
//
//	public Integer getRating() {
//		return rating;
//	}
//
//	public void setRating(Integer rating) {
//		this.rating = rating;
//	}
//
//	public User getUser() {
//		return user;
//	}
//
//	public void setUser(User user) {
//		this.user = user;
//	}
//
//	public String getText() {
//		return text;
//	}
//
//	public void setText(String text) {
//		this.text = text;
//	}
//
//	public String getTimeCreated() {
//		return timeCreated;
//	}
//
//	public void setTimeCreated(String timeCreated) {
//		this.timeCreated = timeCreated;
//	}
//
//	public String getUrl() {
//		return url;
//	}
//
//	public void setUrl(String url) {
//		this.url = url;
//	}
//
}

package com.demo.crescendo.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class ReviewCollection {
	@JsonProperty("reviews")
	private List<Review> reviews;

	@JsonProperty("total")
	private Integer total;

	@JsonProperty("possible_languages")
	private List<String> possibleLanguages;
}

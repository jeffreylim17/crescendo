package com.demo.crescendo.model;

import lombok.Data;

@Data
public class Source {
	private String imageUri;
}

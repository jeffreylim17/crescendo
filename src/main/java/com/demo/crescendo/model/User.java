package com.demo.crescendo.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class User {

	@JsonProperty("id")
	private String id;

	@JsonProperty("profile_url")
	private String profileUrl;

	@JsonProperty("image_url")
	private String imageUrl;

	@JsonProperty("name")
	private String name;

}

package com.demo.crescendo.model;

import lombok.Data;

@Data
public class Vertices {
	private Integer x;
	private Integer y;
}

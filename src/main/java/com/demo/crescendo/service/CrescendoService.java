package com.demo.crescendo.service;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.demo.crescendo.model.APIVisionRequest;
import com.demo.crescendo.model.APIVisionRequestCollection;
import com.demo.crescendo.model.APIVisionResponseCollection;
import com.demo.crescendo.model.FaceAnnotation;
import com.demo.crescendo.model.Feature;
import com.demo.crescendo.model.Image;
import com.demo.crescendo.model.ProcessResponse;
import com.demo.crescendo.model.Response;
import com.demo.crescendo.model.Review;
import com.demo.crescendo.model.ReviewCollection;
import com.demo.crescendo.model.Source;
import com.demo.crescendo.model.User;

@Component
public class CrescendoService {

	private static final Logger LOG = Logger.getLogger(CrescendoService.class.getName());

	@Value("${yelp.review.endpoint}")
	private String yelpEndpoint;

	@Value("${google.api.key}")
	private String apiKey;

	@Value("${bearer.security.token}")
	private String bearerToken;

	public ResponseEntity<ReviewCollection> getReview(String id) {
		LOG.info("getReview initiated ::");
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Accept", "application/json");
		headers.setBearerAuth(bearerToken);

		HttpEntity<ReviewCollection> entity = new HttpEntity<ReviewCollection>(headers);
		String url = MessageFormat.format(yelpEndpoint, id);

		ResponseEntity<ReviewCollection> response = new ResponseEntity<ReviewCollection>(HttpStatus.OK);
		try {
			response = restTemplate.exchange(url, HttpMethod.GET, entity, ReviewCollection.class);
			LOG.info(response.getBody().toString());
		} catch (Exception e) {
			LOG.info("ERR::" + e.getLocalizedMessage());
			response = new ResponseEntity<ReviewCollection>(HttpStatus.BAD_REQUEST);
		}
//		return response;
//		had to re-instantitiate, causing swagger issues
		return new ResponseEntity<ReviewCollection>(response.getBody(), HttpStatus.OK);
	}

	public ResponseEntity<APIVisionResponseCollection> detectFaces(APIVisionRequestCollection body) {
		LOG.info("detectFaces initiated ::");
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		String url = "https://vision.googleapis.com/v1/images:annotate?key=" + apiKey;
		HttpEntity<APIVisionRequestCollection> entity = new HttpEntity<APIVisionRequestCollection>(body, headers);
		ResponseEntity<APIVisionResponseCollection> response = new ResponseEntity<APIVisionResponseCollection>(
				HttpStatus.OK);

		try {
			response = restTemplate.exchange(url, HttpMethod.POST, entity, APIVisionResponseCollection.class);
		} catch (Exception e) {
			response = new ResponseEntity<APIVisionResponseCollection>(HttpStatus.BAD_REQUEST);
			LOG.info("ERR ::" + e.getLocalizedMessage());
		}

		return response;
	}

	public ResponseEntity<ProcessResponse> processReview(String id) {
		LOG.info("processReview initiated ::");
		ResponseEntity<ProcessResponse> processResponseEntity = new ResponseEntity<ProcessResponse>(HttpStatus.OK);
		ProcessResponse processResponse = new ProcessResponse();

		try {

			ResponseEntity<ReviewCollection> reviewCollection = getReview(id);

			APIVisionRequestCollection apiVisionRequestCollection = new APIVisionRequestCollection();
			List<APIVisionRequest> apiVisionRequestList = new ArrayList<APIVisionRequest>();

			List<Feature> featureList = new ArrayList<Feature>();
			Feature feature = new Feature();
			feature.setType("FACE_DETECTION");
			featureList.add(feature);
			APIVisionRequest apiVisionRequest = new APIVisionRequest();
			apiVisionRequest.setFeatures(featureList);

			Image image = new Image();
			Source source = new Source();

			List<Response> response = new ArrayList<Response>();

			for (Review review : reviewCollection.getBody().getReviews()) {
				source.setImageUri(review.getUser().getImageUrl());
				image.setSource(source);

				apiVisionRequest.setImage(image);
				apiVisionRequestList.add(apiVisionRequest);
				apiVisionRequestCollection.setRequests(apiVisionRequestList);

				response.add(responseMapper(detectFaces(apiVisionRequestCollection).getBody(), review.getUser()));

			}
			processResponse.setResponse(response);
			LOG.info(processResponse.toString());
			processResponseEntity = new ResponseEntity<ProcessResponse>(processResponse, HttpStatus.OK);
		} catch (Exception e) {
			LOG.info(e.getLocalizedMessage());
			processResponseEntity = new ResponseEntity<ProcessResponse>(HttpStatus.BAD_REQUEST);
		}

		return processResponseEntity;
	}

	private Response responseMapper(APIVisionResponseCollection body, User user) {
		Response resp = new Response();
		FaceAnnotation faceAnnotation = body.getResponses().get(0).getFaceAnnotations().get(0);
		resp.setAngerLikelihood(faceAnnotation.getAngerLikelihood());
		resp.setJoyLikelihood(faceAnnotation.getJoyLikelihood());
		resp.setSorrowLikelihood(faceAnnotation.getSorrowLikelihood());
		resp.setSurpriseLikelihood(faceAnnotation.getSurpriseLikelihood());
		resp.setUser(user);
		return resp;

	}

}
